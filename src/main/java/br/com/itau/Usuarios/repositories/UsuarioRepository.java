package br.com.itau.Usuarios.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.Usuarios.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{

	public Optional<Usuario> findAllByNome(String nome);
}