package br.com.itau.Usuarios.controllers;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.Usuarios.models.Usuario;
import br.com.itau.Usuarios.services.UsuarioService;


@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired 
	UsuarioService usuarioService;
	
	@GetMapping("/{nome}")
	public Optional<Usuario> buscarUsuarioPorNome(@PathVariable String nome){
		return usuarioService.obterUsuarioPorNome(nome);
	}

	@GetMapping
	public Iterable<Usuario> buscarUsuario(){ 
		return usuarioService.obterUsuario();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity atualizarUsuario(@PathVariable int id, @RequestBody Usuario usuario) {
		boolean resultadoAtualizacao = usuarioService.alterarUsuario(id, usuario);
		
		if (resultadoAtualizacao) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}

	@PostMapping
	public ResponseEntity criarUsuario(@RequestBody Usuario usuarioDados) {
		boolean resultadoInsercao = usuarioService.inserir(usuarioDados);
		
		if (resultadoInsercao) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity deletarUsuario(@PathVariable int id) {
		boolean resultadoDelecao = usuarioService.deletarUsuario(id);
		
		if (resultadoDelecao) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}
	
	@GetMapping("/validar")
	public Map<String, String> validar(Principal principal) {
		Map<String, String> mapa = new HashMap<>();
		mapa.put("name", principal.getName());
		
		return mapa;
	}
}