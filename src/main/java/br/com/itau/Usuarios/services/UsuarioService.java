package br.com.itau.Usuarios.services;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.Usuarios.models.Usuario;
import br.com.itau.Usuarios.repositories.UsuarioRepository;

@Service
public class UsuarioService implements UserDetailsService{

	@Autowired
	UsuarioRepository usuarioRepository;

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	private Usuario usuario = new Usuario();

	public Iterable<Usuario> obterUsuario() {
		return usuarioRepository.findAll();
	}

	public Optional<Usuario> obterUsuarioPorNome(String nome) {
		return usuarioRepository.findAllByNome(nome);
	}

	private Optional<Usuario> obterUsuarioPorId(int id) {
		return usuarioRepository.findById(id);
	}

	public boolean inserir(Usuario usuarioDados) {
		inserirUsuario(usuarioDados);
		return true;
	}

	private void inserirUsuario(Usuario usuarioDados) {
		usuario.setEmail(usuarioDados.getEmail());
		usuario.setNome(usuarioDados.getNome());

		String hash = encoder.encode(usuarioDados.getSenha());

		usuario.setSenha(hash);

		usuarioRepository.save(usuario);
	}

	public boolean alterarUsuario(int id, Usuario usuario) {
		Optional<Usuario> usuarioOptional = obterUsuarioPorId(id);

		usuario.setId(id);

		if (usuarioOptional.isPresent()) {
			usuarioRepository.save(usuario);
			return true;
		}
		return false;
	}

	public boolean deletarUsuario(int id) {
		Optional<Usuario> usuarioOptional = obterUsuarioPorId(id);

		if (usuarioOptional.isPresent()) {
			usuarioRepository.delete(usuarioOptional.get());
			return true;
		}
		return false;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> usuarioOptional = obterUsuarioPorNome(username);
		
		if(!usuarioOptional.isPresent()) {
			throw new UsernameNotFoundException(username);
		}
		
		Usuario usuario = usuarioOptional.get();
		
		return new User(usuario.getNome(), usuario.getSenha(), Collections.emptyList());
	}
}